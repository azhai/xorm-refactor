#!/bin/bash

sed -i 's/runtime\.walltime1/runtime.nanotime/' vendor/github.com/muyo/sno/internal/time.go

sed -i 's/\t\tif isUnsigned/\t\tif isUnsigned \&\& \(colType == "BIT" \|\| strings\.HasSuffix\(colType\, "INT"\)\)/' vendor/xorm.io/xorm/dialects/mysql.go