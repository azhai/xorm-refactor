package config

import (
	"strings"

	"gitee.com/azhai/xorm-refactor/v2/config/dialect"
	"github.com/gomodule/redigo/redis"
	"xorm.io/xorm"
)

type IReverseConfig interface {
	GetReverseTarget(name string) ReverseTarget
	GetConnConfigMap(keys ...string) map[string]ConnConfig
	GetConnConfig(key string) (ConnConfig, bool)
}

type ConnConfig struct {
	DriverName  string             `json:"driver_name" yaml:"driver_name" toml:"driver_name"`
	TablePrefix string             `json:"table_prefix,omitempty" yaml:"table_prefix,omitempty" toml:"table_prefix"`
	LogFile     string             `json:"log_file,omitempty" yaml:"log_file,omitempty" toml:"log_file"`
	Params      dialect.ConnParams `json:"params" yaml:"params" toml:"params"`
}

func (c ConnConfig) ConnectXorm(verbose bool) (*xorm.Engine, error) {
	d := dialect.GetDialectByName(c.DriverName)
	dsn := d.ParseDSN(c.Params)
	engine, err := xorm.NewEngine(c.DriverName, dsn)
	if err == nil {
		engine.ShowSQL(verbose)
	}
	return engine, err
}

func (c ConnConfig) ConnectRedis(verbose bool) (redis.Conn, error) {
	d := new(dialect.Redis)
	addr := d.ParseDSN(c.Params)
	return redis.Dial("tcp", addr, d.GetOptions()...)
}

type IConfigure interface {
	Autofix(params map[string]interface{}, err error) error
}

type Configure struct {
	Connections   map[string]ConnConfig  `json:"connections" yaml:"connections" toml:"connections"`
	ReverseTarget ReverseTarget          `json:"reverse_target" yaml:"reverse_target" toml:"reverse_target"`
	Extras        map[string]interface{} `json:"extras,omitempty" yaml:"extras,omitempty" toml:"extras"`
}

func ReadSettings(fileName, nameSpace string) (*Configure, error) {
	cfg, params := new(Configure), map[string]interface{}{"name_space": nameSpace}
	err := LoadDataFile(cfg, fileName)
	err = cfg.Autofix(params, err)
	return cfg, err
}

func (cfg *Configure) Autofix(params map[string]interface{}, err error) error {
	if err != nil {
		var nameSpace = ""
		if value, ok := params["name_space"]; ok {
			nameSpace = value.(string)
		}
		cfg.ReverseTarget = DefaultMixinReverseTarget(nameSpace)
	} else if len(cfg.Connections) > 0 {
		cfg.RemovePrivates()
	}
	return err
}

func (cfg *Configure) RemovePrivates() {
	for key := range cfg.Connections {
		if strings.HasPrefix(key, "_") {
			delete(cfg.Connections, key)
		}
	}
}

func (cfg Configure) GetReverseTarget(name string) ReverseTarget {
	if name == "*" || name == cfg.ReverseTarget.Language {
		return cfg.ReverseTarget
	}
	return ReverseTarget{OutputDir: "/dev/null"}
}

func (cfg Configure) GetConnConfigMap(keys ...string) map[string]ConnConfig {
	if len(keys) == 0 {
		return cfg.Connections
	}
	result := make(map[string]ConnConfig)
	for _, k := range keys {
		if c, ok := cfg.Connections[k]; ok {
			result[k] = c
		}
	}
	return result
}

func (cfg Configure) GetConnConfig(key string) (ConnConfig, bool) {
	if c, ok := cfg.Connections[key]; ok {
		return c, true
	}
	return ConnConfig{}, false
}
