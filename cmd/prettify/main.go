package main

import (
	"flag"
	"fmt"
	"regexp"
	"strings"

	"gitee.com/azhai/xorm-refactor/v2/rewrite"
	"gitee.com/azhai/xorm-refactor/v2/utils"
)

var (
	sourceDir   string // 代码文件目录
	includeVend bool   // 包括vendor目录
	verbose     bool   // 列出文件名称
)

// 解析参数
func init() {
	flag.StringVar(&sourceDir, "d", "./", "代码文件目录")
	flag.BoolVar(&includeVend, "a", false, "包括vendor目录")
	flag.BoolVar(&verbose, "v", false, "列出文件名称")
	flag.Parse()

	if flag.NArg() > 0 {
		sourceDir = flag.Arg(0)
	}
	if strings.Contains(sourceDir, "vendor/") {
		includeVend = true
	}
}

// 运行命令
func main() {
	files, err := utils.FindFiles(sourceDir, ".go")
	if err != nil {
		fmt.Println("xxx", err.Error())
		return
	}

	var changed bool
	re := regexp.MustCompile(`\.(git|github|idea|vscode)/`)
	for f := range files {
		if re.MatchString(f) {
			continue
		}
		if !includeVend && strings.Contains(f, "vendor/") {
			continue
		}
		changed, err = rewrite.PrettifyGolangFile(f, true)
		if err != nil {
			fmt.Println("!!!", f, "=>", err.Error())
			break
		}
		if verbose {
			if changed {
				fmt.Println("+", f)
			} else {
				fmt.Println("-", f)
			}
		}
	}
}
