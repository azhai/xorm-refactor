package main

import (
	"os"

	refactor "gitee.com/azhai/xorm-refactor/v2"
	"gitee.com/azhai/xorm-refactor/v2/config"
	"gitee.com/azhai/xorm-refactor/v2/utils"
	"github.com/urfave/cli/v2"
)

func main() {
	app := &cli.App{
		Version: refactor.VERSION,
		Usage:   "从数据库导出对应的Model代码",
		Action:  ReverseAction,
	}
	app.Flags = []cli.Flag{
		&cli.StringFlag{
			Name:    "file",
			Aliases: []string{"c", "f"},
			Usage:   "配置文件路径",
			Value:   "settings.yml",
		},
		&cli.StringFlag{
			Name:    "namespace",
			Aliases: []string{"ns"},
			Usage:   "项目NameSpace",
			Value:   "xorm-refactor",
		},
		&cli.BoolFlag{
			Name:    "override",
			Aliases: []string{"o"},
			Usage:   "覆盖配置文件",
		},
		&cli.BoolFlag{
			Name:    "verbose",
			Aliases: []string{"vv"},
			Usage:   "输出详细信息",
		},
	}
	err := app.Run(os.Args)
	if err != nil {
		panic(err)
	}
}

func ReverseAction(ctx *cli.Context) error {
	nameSpace := ctx.String("namespace")
	configFile := utils.GetFirstFile([]string{
		ctx.String("file"), "settings.yml", "settings.json",
	}, 0)
	settings, err := config.ReadSettings(configFile, nameSpace)
	if err != nil {
		return err
	}

	if override := ctx.Bool("override"); override {
		_ = config.SaveDataFile(settings, configFile)
	}

	names := ctx.Args().Slice()
	verbose := ctx.Bool("verbose")
	err = refactor.ExecReverseSettings(settings, verbose, names...)
	return err
}
