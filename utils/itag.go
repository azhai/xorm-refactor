package utils

import (
	"reflect"
	"strconv"
	"strings"
	"sync"
)

const ( // 约定大于配置
	TAG_KEY_SEP   = "/"
	TAG_FIELD_SEP = ","
)

type Word string

// SkipAnyChar 忽略开头字符
func (w Word) SkipAnyChar(chars string) Word {
	i := 0
	for i < len(w) && strings.ContainsRune(chars, rune(w[i])) {
		i++
	}
	return w[i:]
}

// MatchSubString 寻找字符第一次出现的位置，类似IndexAny
func (w Word) MatchSubString(subs string) (string, Word) {
	i := 0
	for i < len(w) && w[i] >= 0x20 && w[i] != 0x7f && w[i] != subs[0] {
		if w[i] == '\\' { //不要匹配同名转义字符，例如目标是"就要跳过\"
			i++
		}
		i++
	}
	if i == 0 || i >= len(w) || !strings.HasPrefix(string(w[i:]), subs) {
		return "", ""
	}
	return string(w[:i]), w[i+len(subs):]
}

// Itag Yet another StructTag
type Itag struct {
	alias   map[string]string
	data    map[string]string
	changed bool
	lock    sync.RWMutex
	reflect.StructTag
}

func NewItag() *Itag {
	it := new(Itag)
	it.alias = make(map[string]string)
	it.data = make(map[string]string)
	return it
}

// Burnish 保留部分tags
func (it *Itag) Burnish(tag reflect.StructTag, names ...string) {
	it.StructTag, it.changed = tag, true
	for _, key := range names {
		if value, ok := tag.Lookup(key); ok {
			it.Append(key, value)
		}
	}
}

// Parse 解析全部tags
func (it *Itag) Parse(tag reflect.StructTag) {
	it.StructTag, it.changed = tag, true
	word, key, chunk := Word(tag), "", ""
	for word != "" {
		if word = word.SkipAnyChar(" "); word == "" {
			break
		}
		if key, word = word.MatchSubString(":\""); key == "" {
			break
		}
		chunk, word = word.MatchSubString("\"")
		value, err := strconv.Unquote("\"" + chunk + "\"")
		if err != nil {
			break
		}
		it.Append(key, value)
	}
}

// Build 转为字符串格式
func (it Itag) Build(data []byte, name, value string) []byte {
	data = append(data, []byte(name)...)
	data = append(data, byte(':'), byte('"'))
	data = append(data, []byte(value)...)
	data = append(data, byte('"'), byte(' '))
	return data
}

// String 转为字符串格式
func (it Itag) String() string {
	if !it.changed {
		return string(it.StructTag)
	}
	var data []byte
	if len(it.alias) > len(it.data) {
		for _, key := range SortedKeys(it.alias) {
			if value := it.Get(key); value != "" {
				data = it.Build(data, key, value)
			}
		}
	} else {
		for _, name := range SortedKeys(it.data) {
			data = it.Build(data, name, it.data[name])
		}
	}
	result := strings.TrimSpace(string(data))
	it.StructTag = reflect.StructTag(result)
	it.changed = false
	return result
}

// Get returns the value associated with key in the tag string
func (it Itag) Get(key string) string {
	v, _ := it.Lookup(key)
	return v
}

// Lookup Returns a tag from the tag data
func (it Itag) Lookup(key string) (string, bool) {
	it.lock.RLock()
	defer it.lock.RUnlock()
	if name, ok := it.alias[key]; ok {
		key = name
	}
	value, ok := it.data[key]
	return value, ok
}

// Append Sets a tag in the tag data map
func (it *Itag) Append(key, value string) {
	it.lock.Lock()
	defer it.lock.Unlock()
	names := strings.Split(key, TAG_KEY_SEP)
	name := names[len(names)-1]
	for _, key = range names {
		it.alias[key] = name
	}
	it.changed, it.data[name] = true, value
}

// Delete Deletes a tag
func (it *Itag) Delete(key string) {
	it.lock.Lock()
	defer it.lock.Unlock()
	delete(it.alias, key)
	delete(it.data, key)
	it.changed = true
}
