package utils

import (
	"fmt"
	"io"
	"io/ioutil"
	"os"
	"os/exec"
	"path/filepath"
	"strings"
)

const (
	DEFAULT_DIR_MODE  = 0o755
	DEFAULT_FILE_MODE = 0o644
)

// FileSize detect if file exists
// -1, false 不合法的路径
// 0, false 路径不存在
// -1, true 存在文件夹
// >=0, true 文件并存在
func FileSize(path string) (int64, bool) {
	if path == "" {
		return -1, false
	}
	info, err := os.Stat(path)
	if err != nil && os.IsNotExist(err) {
		return 0, false
	}
	var size = int64(-1)
	if info.IsDir() == false {
		size = info.Size()
	}
	return size, true
}

func CreateFile(path string) (fp *os.File, err error) {
	// create dirs if file not exists
	if dir := filepath.Dir(path); dir != "." {
		err = os.MkdirAll(dir, DEFAULT_DIR_MODE)
	}
	if err == nil {
		flag := os.O_RDWR | os.O_CREATE | os.O_TRUNC
		fp, err = os.OpenFile(path, flag, DEFAULT_FILE_MODE)
	}
	return
}

func OpenFile(path string, readonly, append bool) (fp *os.File, size int64, err error) {
	var exists bool
	size, exists = FileSize(path)
	if size < 0 {
		err = fmt.Errorf("path is directory or illegal")
		return
	}
	if exists {
		flag := os.O_RDWR
		if readonly {
			flag = os.O_RDONLY
		} else if append {
			flag |= os.O_APPEND
		}
		fp, err = os.OpenFile(path, flag, DEFAULT_FILE_MODE)
	} else if readonly == false {
		fp, err = CreateFile(path)
	}
	return
}

// MkdirForFile 为文件路径创建目录
func MkdirForFile(path string, force bool) (size int64) {
	var exists bool
	if !force {
		if size, exists = FileSize(path); size < 0 {
			return
		}
	}
	if force || !exists {
		dir := filepath.Dir(path)
		if err := os.MkdirAll(dir, DEFAULT_DIR_MODE); err != nil {
			size = -1
		}
	}
	return
}

// CopyDir 通过Bash命令复制整个目录，只能运行于Linux或MacOS
// 当dst结尾带斜杠时，复制为dst下的子目录
func CopyDir(src, dst string) (err error) {
	if length := len(src); src[length-1] == '/' {
		src = src[:length-1] //去掉结尾的斜杠
	}
	info, err := os.Stat(src)
	if err != nil || !info.IsDir() {
		return
	}
	err = exec.Command("cp", "-rf", src, dst).Run()
	return
}

// CopyFile copies the contents of the file named src to the file named
// by dst. The file will be created if it does not already exist. If the
// destination file exists, all it's contents will be replaced by the contents
// of the source file.
func CopyFile(src, dst string) (err error) {
	in, err := os.Open(src)
	if err != nil {
		return
	}
	defer in.Close()
	out, err := os.Create(dst)
	if err != nil {
		return
	}
	defer func() {
		cerr := out.Close()
		if err == nil {
			err = cerr
		}
	}()
	if _, err = io.Copy(out, in); err != nil {
		return
	}
	err = out.Sync()
	return
}

// ListFiles 找出目录下的文件，不含子目录
func ListFiles(dir, ext string) (map[string]os.FileInfo, error) {
	var result = make(map[string]os.FileInfo)
	files, err := ioutil.ReadDir(dir)
	if err != nil {
		return result, err
	}
	for _, file := range files {
		fname := file.Name()
		if ext != "" && !strings.HasSuffix(fname, ext) {
			continue
		}
		fname = filepath.Join(dir, fname)
		result[fname] = file
	}
	return result, nil
}

// FindFiles 遍历目录下的文件，递归
func FindFiles(dir, ext string) (map[string]os.FileInfo, error) {
	result := make(map[string]os.FileInfo)
	err := filepath.Walk(dir, func(path string, info os.FileInfo, err error) error {
		if err == nil && info.Mode().IsRegular() {
			if ext == "" || strings.HasSuffix(info.Name(), ext) {
				result[path] = info
			}
		}
		return err
	})
	return result, err
}

// GetFirstFile 逐个尝试，找出第一个存在的文件
func GetFirstFile(fileNames []string, minSize int64) string {
	if len(fileNames) == 0 {
		return ""
	}
	for _, f := range fileNames { // 找到第一个存在的文件
		size, exists := FileSize(f)
		if exists && size >= minSize {
			return f
		}
	}
	return ""
}
